<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ArrayDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    
    
    
    public function actionEjercicio1(){
        
        $datos=[];
        
        for ($index = 0; $index < 100; $index++) {
            
            $datos[$index]=$index;
        }
        
        
        return $this->render('ejercicio1',[
            
            "datos"=>$datos
        ]);
    }
    
    public function actionEjercicio2(){
        
        
        $visitas=[
            
            [
                "mes"=>"Junio",
                "visitas"=>10001
                
            ],
            
            [
                "mes"=>"Mayo",
                "visitas"=>10000
            ],
            [
               "mes"=>"Julio",
                "visitas"=>11000000 
            ],
            [
                "mes"=>"Agosto",
                "visitas"=>15550000
            ],
            
            
            
            
            
        ];
        
         return $this->render('ejercicio2',[
             
             "visitas"=>$visitas
        
    ]);
    }
    
    public function actionEjercicio3(){
        
        
        $visitas=[
            
            [
                "mes"=>"Junio",
                "visitas"=>10001
                
            ],
            
            [
                "mes"=>"Mayo",
                "visitas"=>10000
            ],
            [
               "mes"=>"Julio",
                "visitas"=>11000000 
            ],
            [
                "mes"=>"Agosto",
                "visitas"=>15550000
            ],
            
            
            
            //mostrar el contenido del array en una vista, utilizando un gridview
            
        ];
        //en el controlador se crea el dataprovider que se usa en la vista
        $dataProvider = new ArrayDataProvider([
            "allModels" => $visitas
                
                ]);
        
        
        
         return $this->render('ejercicio3',[
             
             "dataProvider"=>$dataProvider,
        
    ]);
    }
    
    public function actionEjercicio4(){
        
        //crear array enumerado de arrays asociativos
        
        $ventas=[
            
            [
                "id"=>1,
                "cantidad"=>100,
                "producto"=>56,
                "preciototal"=>560,
                "poblacion"=>"santander",
            ],
            [
                "id"=>2,
                "cantidad"=>200,
                "producto"=>506,
                "preciototal"=>460,
                "poblacion"=>"sant-ander",
            ],
            [
                "id"=>3,
                "cantidad"=>300,
                "producto"=>56000,
                "preciototal"=>450,
                "poblacion"=>"valladoli",
            ]
            
        ];
        
         $dataProvider = new ArrayDataProvider([
            "allModels" => $ventas
                
                ]);
        
        
        
         return $this->render('ejercicio4',[
             
             "dataProvider"=>$dataProvider,
        
    ]);
        
    }
    
    public function actionEjercicio5(){
        
         $visitas=[
            
            [
                "mes"=>"Junio",
                "visitas"=>10001
                
            ],
            
            [
                "mes"=>"Mayo",
                "visitas"=>10000
            ],
            [
               "mes"=>"Julio",
                "visitas"=>11000000 
            ],
            [
                "mes"=>"Agosto",
                "visitas"=>15550000
            ],
            
            
            
            //mostrar el contenido del array en una vista, utilizando un Listview
            
        ];
        //en el controlador se crea el dataprovider que se usa en la vista
        $dataProvider = new ArrayDataProvider([
            "allModels" => $visitas
                
                ]);
        
        
        
         return $this->render('ejercicio5',[
             
             "dataProvider"=>$dataProvider,
             
        
    ]);
        
    }
    
    public function actionEjercicio6 (){
        
        $ventas=[
            
            [
                "id"=>1,
                "cantidad"=>100,
                "producto"=>56,
                "preciototal"=>560,
                "poblacion"=>"santander",
            ],
            [
                "id"=>2,
                "cantidad"=>200,
                "producto"=>506,
                "preciototal"=>460,
                "poblacion"=>"sant-ander",
            ],
            [
                "id"=>3,
                "cantidad"=>300,
                "producto"=>56000,
                "preciototal"=>450,
                "poblacion"=>"valladoli",
            ]
            
        
            
            
            
            //mostrar el contenido del array en una vista, utilizando un Listview
            
        ];
        //en el controlador se crea el dataprovider que se usa en la vista
        $dataProvider = new ArrayDataProvider([
            "allModels" => $ventas
                
                ]);
        
        
        
         return $this->render('ejercicio6',[
             
             "dataProvider"=>$dataProvider,
             
             ]);
        
    }
    
    public function actionEjercicio7(){
        
        //api key
        //77ccbab0fb0749d6b8c581a5373c71e1
        
       
        
       $url="https://newsapi.org/v2/everything?q=php&language=es&apiKey=77ccbab0fb0749d6b8c581a5373c71e1";
       
     $contenido=file_get_contents($url);
     
     $resultado=json_decode($contenido);
     
     $noticias=$resultado->articles;
     
    // var_dump($noticias);
     
     $dataProvider = new ArrayDataProvider([
            "allModels" => $noticias
                
                ]);
        
        
        
         return $this->render("ejercicio7",[
             
             "dataProvider"=>$dataProvider,
             
             ]);
        
    }
    
    public function actionEjercicio8(){
        
        $url="https://newsapi.org/v2/top-headlines?language=es&apiKey=77ccbab0fb0749d6b8c581a5373c71e1";
        
        $contenido= file_get_contents($url);
        $resultado= json_decode($contenido);
        $noticias=$resultado->articles;
        
        $dataProvider= new ArrayDataProvider([
            "allModels"=>$noticias
            
        ]);
        
        return $this->render("ejercicio8",[
            
            "dataProvider"=>$dataProvider,
        ]);
    }
    
//   public function actionFotos(){     //asi cargarmos fotos manualmente
//       
//      $f1 = new \app\models\Fotos();
//      
//      $f1->id=2;
//      $f1->ruta="foto2.jpg";
//      $f1->idprenda=2;
//      $f1->save(); //metodo que utiliza activerecord para grabar los datos
//       
//   }
    
    
    public function actionEjercicio9(){
        //mostrar la siguiente consulta en un gridview
        
        //activequery
        $query= \app\models\Fotos::find();
        
       
        $dataProvider = new \yii\data\ActiveDataProvider([  //un query se hace con un objeto de la clase activedataprovider
            "query" => $query 
                
                
            
        ]);
        
      return  $this->render("ejercicio9",[
            
            "dataProvider"=>$dataProvider
        ]);
        
        
        {
            
            
                    
            
    }
        
        
    }
    public function actionEjercicio10(){
        //activequery
        $query= \app\models\Fotos::find();
        
        
        
        //array de activeRecords (array de modelos) pero no tengo un activedataprovider, puedo modificar los registros desde el array
        
        $registros=$query->all();
        
        $registros[0]->idprenda= random_int(1, 10);  //asi al ejecutar cambia el id y lo pone aleatoriod
        $registros[0]->save();
        
        
        return $this->render("ejercicio10",[
            
            "query"=>$query,
            "registros"=>$registros
        
        ]);
     
        
        
        
    }
    public function actionEjercicio11(){
        //activequery
        $query= \app\models\Fotos::find();
        
        //actverecrd (modelo)
        $activeRecord=$query->one(); // pero accedemos solo a un registro, entonces lo hacemos con detailview, que debe ser pasado desde un modelo.
        
        $activeRecords=$query->all();// devolvería todos los registros
        
        $activeRecord=$activeRecords[1]; //asi me pongo en la posicion qye yo quiera
        
        return $this->render("ejercicio11",[
            
           "model"=>$activeRecord, 
        ]);
        
        
        
        
        
        
        
        
        
    }
    public function actionEjercicio12(){
      
        //esto no es con active record, es un array de arrays, sin modelo,
        
       $resultados= \yii::$app  
               ->db //objeto de acceso a datos
               ->createCommand("select*from fotos")// comando sql preparado para ejecutarse, este ejemplo es para consultas complejas, para consultas simples es mejor usar el ejemplo9
               ->queryAll();//consulta ejecutada
       
       //si modifico alguno de los registros no afecta a la tabla, para modificar es necesario el activerecord
        
       $dataProvider = new ArrayDataProvider([
           
          "allModels"=>$resultados
               
       ]);
        
       return $this->render("ejercicio12",[
           
           "dataProvider"=>$dataProvider
       ]);
    }
    
    public function actionEjercicio13(){
        //mostrar la siguiente consulta en un gridview
        //select*from fotos
        
        $dataProvider=new \yii\data\SqlDataProvider([ //utilizamos el visualizador de datos que nos falta
            
            "sql"=>"select * from fotos"
            
            
        ]);
        return $this->render("ejercicio12",[  //lo pasamos a la misma vista del ejercicio 12 es igual
           
           "dataProvider"=>$dataProvider
        ]);
    }
    
    public function actionEjercicio14(){
        //listado de todas las prendas utilizando create command.
        //select * from prendas
        //utilizar para mostrar los resultados un gridview.
        
        $resultados= \yii::$app->db->createCommand("select*from prendas")
               ->queryAll();
       
       
        
       $dataProvider = new ArrayDataProvider([
           
          "allModels"=>$resultados
               
       ]);
        
       return $this->render("ejercicio14",[
           
           "dataProvider"=>$dataProvider
        ]);
        
    }
    
    public function actionEjercicio15(){
        //listado de todas las prndas utilizando un sqldataprovider
        //select * from prendas
        //utilizar para mostrar los resultados un gridview.
        
        $dataProvider = new \yii\data\SqlDataProvider([
            "sql"=>"select * from prendas where portada=1"  //asi definimos solo los que tienen la portada 0
            
        ]);
        
        return $this->render("ejercicio15",[
           
            "dataProvider"=>$dataProvider
        ]);
    }
    
    public function actionEjercicio16(){
        //listado de todas las prendas utilizando un activedataprovider (necesario un activequery)
        //select * from prendas
        //utilizar para mostrar los resultados un gridview.
        $query=\app\models\Prendas::find();
        
       $dataProvider = new \yii\data\ActiveDataProvider([
           
           "query"=>$query
       ]);
        
        
        return $this->render("ejercicio16",[
            
           "dataProvider"=>$dataProvider
        ]);
        
        
        
    }
}
