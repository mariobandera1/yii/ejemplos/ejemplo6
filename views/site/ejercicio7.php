<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    
    "dataProvider"=>$dataProvider,
    "columns"=>[
        ['class' => 'yii\grid\SerialColumn'], //numero de orden, esto no esta en los datos recibidos sacado de besourcecode.
        "author",
        "title",
        [
            'attribute'=>'description',  //esto es para cambiar el titulo de algo, se coge description y en los corchetes de dentro en label se pone ya en castellano
            'label'=>'descripcion',
        ],
        
        //campo de imagen, salen dos fotos porque esta puesto dos veces
        
        
        [
          "label"=>"Imagen",
            
            "content"=> function ($dato){  //se puede poner de dos formas, con value o con content.
    
                return Html::img($dato->urlToImage,["width"=>200]);
    
            }
        ],
        [
          "label"=>"Imagen",
            "format"=>"raw",
            "value"=> function ($dato){  //value puede ponerse como content, pero al poner value lo pone como un texto plano, en este caso hay que poner el atritubo format=>raw
    
                return Html::img($dato->urlToImage,["width"=>200]);
    
            }
        ],
                               
                
          //campo de url, campo que contiene una url y quiero colocar un boton que me permita ir a la URL  
                
                
                [
                    "label"=>"ir a la noticia",
                    "content"=> function ($dato){
            return Html::a("ver noticia completa",$dato->url,["class"=>"btn btn-dark"]);
                    }
                    ]
        
        
                
    ],
    
    'tableOptions' =>['class' => 'table table-success'],
    'layout'=>"\n{pager}\n{summary}\n{items}\n{pager}\n{summary}"
          
    
    
]);