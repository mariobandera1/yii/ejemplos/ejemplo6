<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    "dataProvider" => $dataProvider,
    "columns" => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'publishedAt',
            'label' => 'fecha de publicacion',
        ],
        [
            "label" => "imagen",
            "content" => function ($dato) {

                return Html::img($dato->urlToImage, ["width" => 200]);
            }
        ],
        [
            'attribute' => 'title',
            'label' => 'titulo',
        ],
        [
            'attribute' => 'author',
            'label' => 'autor de la noticia',
        ],
        [
            "label" => "ir a la noticia",
            "content" => function ($dato) {
                return Html::a("ver noticia completa", $dato->url, ["class" => "btn btn-danger"]);
            }
        ],
    ],
    'tableOptions' => ['class' => 'table table-success'],
    'layout' => "\n{pager}\n{summary}\n{items}\n{pager}\n{summary}"
]);
