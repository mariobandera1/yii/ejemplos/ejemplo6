<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Caracteristicas $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="caracteristicas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'prenda')->textInput() ?>

    <?= $form->field($model, 'caracteristica')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
